﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HomeUtility
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.CostLabel = New System.Windows.Forms.Label()
        Me.ApplianceLabel = New System.Windows.Forms.Label()
        Me.PowerLabel = New System.Windows.Forms.Label()
        Me.HoursLabel = New System.Windows.Forms.Label()
        Me.CostInput = New System.Windows.Forms.TextBox()
        Me.PowerInput = New System.Windows.Forms.TextBox()
        Me.HoursInput = New System.Windows.Forms.TextBox()
        Me.MonthlyLabel = New System.Windows.Forms.Label()
        Me.CalculateLabel = New System.Windows.Forms.Label()
        Me.CalculateButton = New System.Windows.Forms.Button()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DaysInput = New System.Windows.Forms.TextBox()
        Me.ApplianceBox = New System.Windows.Forms.ComboBox()
        Me.WelcomeLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DailyLabel = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.ImageLocation = "C:\Users\Kendra Marie\Documents\Visual Studio 2013\Projects\HomeUtility\HomeUtili" & _
    "ty\Utilities.jpg"
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(299, 262)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'CostLabel
        '
        Me.CostLabel.AutoSize = True
        Me.CostLabel.Location = New System.Drawing.Point(320, 58)
        Me.CostLabel.Name = "CostLabel"
        Me.CostLabel.Size = New System.Drawing.Size(132, 13)
        Me.CostLabel.TabIndex = 1
        Me.CostLabel.Text = "Current Cost per kW-Hour:"
        '
        'ApplianceLabel
        '
        Me.ApplianceLabel.AutoSize = True
        Me.ApplianceLabel.Location = New System.Drawing.Point(395, 84)
        Me.ApplianceLabel.Name = "ApplianceLabel"
        Me.ApplianceLabel.Size = New System.Drawing.Size(57, 13)
        Me.ApplianceLabel.TabIndex = 2
        Me.ApplianceLabel.Text = "Appliance:"
        '
        'PowerLabel
        '
        Me.PowerLabel.AutoSize = True
        Me.PowerLabel.Location = New System.Drawing.Point(324, 111)
        Me.PowerLabel.Name = "PowerLabel"
        Me.PowerLabel.Size = New System.Drawing.Size(129, 13)
        Me.PowerLabel.TabIndex = 3
        Me.PowerLabel.Text = "Power Needed (in Watts):"
        '
        'HoursLabel
        '
        Me.HoursLabel.AutoSize = True
        Me.HoursLabel.Location = New System.Drawing.Point(354, 137)
        Me.HoursLabel.Name = "HoursLabel"
        Me.HoursLabel.Size = New System.Drawing.Size(98, 13)
        Me.HoursLabel.TabIndex = 4
        Me.HoursLabel.Text = "Hours Used (Daily):"
        '
        'CostInput
        '
        Me.CostInput.Location = New System.Drawing.Point(458, 55)
        Me.CostInput.Name = "CostInput"
        Me.CostInput.Size = New System.Drawing.Size(100, 20)
        Me.CostInput.TabIndex = 5
        '
        'PowerInput
        '
        Me.PowerInput.Location = New System.Drawing.Point(458, 108)
        Me.PowerInput.Name = "PowerInput"
        Me.PowerInput.Size = New System.Drawing.Size(100, 20)
        Me.PowerInput.TabIndex = 7
        '
        'HoursInput
        '
        Me.HoursInput.Location = New System.Drawing.Point(458, 134)
        Me.HoursInput.Name = "HoursInput"
        Me.HoursInput.Size = New System.Drawing.Size(100, 20)
        Me.HoursInput.TabIndex = 8
        '
        'MonthlyLabel
        '
        Me.MonthlyLabel.AutoSize = True
        Me.MonthlyLabel.Location = New System.Drawing.Point(334, 239)
        Me.MonthlyLabel.Name = "MonthlyLabel"
        Me.MonthlyLabel.Size = New System.Drawing.Size(119, 13)
        Me.MonthlyLabel.TabIndex = 9
        Me.MonthlyLabel.Text = "Projected Monthly Cost:"
        '
        'CalculateLabel
        '
        Me.CalculateLabel.AutoSize = True
        Me.CalculateLabel.Location = New System.Drawing.Point(466, 239)
        Me.CalculateLabel.Name = "CalculateLabel"
        Me.CalculateLabel.Size = New System.Drawing.Size(34, 13)
        Me.CalculateLabel.TabIndex = 10
        Me.CalculateLabel.Text = "00.00"
        '
        'CalculateButton
        '
        Me.CalculateButton.Location = New System.Drawing.Point(458, 186)
        Me.CalculateButton.Name = "CalculateButton"
        Me.CalculateButton.Size = New System.Drawing.Size(75, 23)
        Me.CalculateButton.TabIndex = 10
        Me.CalculateButton.Text = "Calculate"
        Me.CalculateButton.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Location = New System.Drawing.Point(539, 186)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(75, 23)
        Me.ExitButton.TabIndex = 11
        Me.ExitButton.Text = "Exit"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(362, 163)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Days This Month:"
        '
        'DaysInput
        '
        Me.DaysInput.Location = New System.Drawing.Point(458, 160)
        Me.DaysInput.Name = "DaysInput"
        Me.DaysInput.Size = New System.Drawing.Size(100, 20)
        Me.DaysInput.TabIndex = 9
        '
        'ApplianceBox
        '
        Me.ApplianceBox.FormattingEnabled = True
        Me.ApplianceBox.Items.AddRange(New Object() {"Air Conditioner", "Television"})
        Me.ApplianceBox.Location = New System.Drawing.Point(458, 81)
        Me.ApplianceBox.Name = "ApplianceBox"
        Me.ApplianceBox.Size = New System.Drawing.Size(121, 21)
        Me.ApplianceBox.TabIndex = 6
        Me.ApplianceBox.Text = "Please Choose ONE"
        '
        'WelcomeLabel
        '
        Me.WelcomeLabel.AutoSize = True
        Me.WelcomeLabel.Location = New System.Drawing.Point(350, 22)
        Me.WelcomeLabel.Name = "WelcomeLabel"
        Me.WelcomeLabel.Size = New System.Drawing.Size(224, 13)
        Me.WelcomeLabel.TabIndex = 15
        Me.WelcomeLabel.Text = "Welcome to the Home Utility Auditing Program"
        Me.WelcomeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(455, 239)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "$"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(348, 217)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Projected Daily Cost:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(455, 217)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(13, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "$"
        '
        'DailyLabel
        '
        Me.DailyLabel.AutoSize = True
        Me.DailyLabel.Location = New System.Drawing.Point(466, 217)
        Me.DailyLabel.Name = "DailyLabel"
        Me.DailyLabel.Size = New System.Drawing.Size(34, 13)
        Me.DailyLabel.TabIndex = 19
        Me.DailyLabel.Text = "00.00"
        '
        'HomeUtility
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 262)
        Me.Controls.Add(Me.DailyLabel)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.WelcomeLabel)
        Me.Controls.Add(Me.ApplianceBox)
        Me.Controls.Add(Me.DaysInput)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.CalculateButton)
        Me.Controls.Add(Me.CalculateLabel)
        Me.Controls.Add(Me.MonthlyLabel)
        Me.Controls.Add(Me.HoursInput)
        Me.Controls.Add(Me.PowerInput)
        Me.Controls.Add(Me.CostInput)
        Me.Controls.Add(Me.HoursLabel)
        Me.Controls.Add(Me.PowerLabel)
        Me.Controls.Add(Me.ApplianceLabel)
        Me.Controls.Add(Me.CostLabel)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "HomeUtility"
        Me.Text = "Home Utility Auditing Program by Robert Vicars"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents CostLabel As System.Windows.Forms.Label
    Friend WithEvents ApplianceLabel As System.Windows.Forms.Label
    Friend WithEvents PowerLabel As System.Windows.Forms.Label
    Friend WithEvents HoursLabel As System.Windows.Forms.Label
    Friend WithEvents CostInput As System.Windows.Forms.TextBox
    Friend WithEvents PowerInput As System.Windows.Forms.TextBox
    Friend WithEvents HoursInput As System.Windows.Forms.TextBox
    Friend WithEvents MonthlyLabel As System.Windows.Forms.Label
    Friend WithEvents CalculateLabel As System.Windows.Forms.Label
    Friend WithEvents CalculateButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DaysInput As System.Windows.Forms.TextBox
    Friend WithEvents ApplianceBox As System.Windows.Forms.ComboBox
    Friend WithEvents WelcomeLabel As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DailyLabel As System.Windows.Forms.Label

End Class
