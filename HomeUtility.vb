﻿Public Class HomeUtility
    'Establish Variables

    Dim CostStr As String
    Dim PowerStr As String
    Dim HoursStr As String
    Dim DaysStr As String

    Dim Cost As Double
    Dim Power As Double
    Dim Hours As Double
    Dim Days As Double
    Dim Final As Double
    Dim Daily As Double

    Dim Item As String
    Dim TV As String
    Dim AC As String


    Private Sub ExitButton_Click(sender As Object, e As EventArgs) Handles ExitButton.Click
        'Exit the Program

        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles CostInput.TextChanged
        'Establish input as Cost Variable

        CostStr = CostInput.Text
    End Sub

    Private Sub PowerInput_TextChanged(sender As Object, e As EventArgs) Handles PowerInput.TextChanged
        'Establish input as Power Variable

        PowerStr = PowerInput.Text
    End Sub

    Private Sub HoursInput_TextChanged(sender As Object, e As EventArgs) Handles HoursInput.TextChanged
        'Establish input as Hours Variable

        HoursStr = HoursInput.Text
    End Sub

    Private Sub DaysInput_TextChanged(sender As Object, e As EventArgs) Handles DaysInput.TextChanged
        'Establish input as Days Variable

        DaysStr = DaysInput.Text
    End Sub

    Private Sub CalculateButton_Click(sender As Object, e As EventArgs) Handles CalculateButton.Click
        
        'Compute Calculation

        Cost = CostStr
        Power = PowerStr
        Hours = HoursStr
        Days = DaysStr
        Final = Cost * (Power / 1000) * Hours * Days
        Daily = Cost * (Power / 1000) * Hours

        CalculateLabel.Text = Final
        DailyLabel.Text = Daily

        'Error Checking
        If Cost > 0.25 Or Cost < 0.1 Then
            MessageBox.Show("Please input a cost value between 0.10 and 0.25.")
            CalculateLabel.Text = "Error: Cost."
            DailyLabel.Text = "Error: Cost"
        End If

        'For Each Item As String In ApplianceBox.SelectedItem
        If Item = AC And 3000 > Power Or Item = AC And 5000 < Power Then
            MessageBox.Show("The average Air Conditioner uses between 3000 and 5000 Watts of Power.")
            CalculateLabel.Text = "Error: Air Conditioner or Power."
            DailyLabel.Text = "Error: Air Conditioner or Power."
        ElseIf Item = TV And 200 > Power Or Item = TV And 500 < Power Then
            MessageBox.Show("The average Television uses between 200 and 500 Watts of Power.")
            CalculateLabel.Text = "Error: Television or Power."
            DailyLabel.Text = "Error: Television or Power."
        End If
        'Next

        If Hours < 0 Or Hours > 24 Then
            MessageBox.Show("The hours must be between 0 and 24.")
            CalculateLabel.Text = "Error: Hours."
            DailyLabel.Text = "Error: Hours"
        End If

        If Days < 28 Or Days > 31 Then
            MessageBox.Show("The days must be between 28 and 31.")
            CalculateLabel.Text = "Error: Days."
        End If

    End Sub

    Private Sub ApplianceBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ApplianceBox.SelectedIndexChanged

        AC = ApplianceBox.SelectedItem(0)
        TV = ApplianceBox.SelectedItem(1)

    End Sub

    Private Sub DailyLabel_Click(sender As Object, e As EventArgs) Handles DailyLabel.Click
        
    End Sub

    Private Sub CalculateLabel_Click(sender As Object, e As EventArgs) Handles CalculateLabel.Click

        

    End Sub
End Class
